<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Authentication | Form</title>

    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
    <form method="POST">
        <h1>Login</h1>

        <div class="body">
            <div class="form-group">
                <label for="email">Email</label>

                <input id="email" name="email" type="email">
            </div>

            <div class="form-group">
                <label for="password">Password</label>

                <input id="password" name="password" type="password">
            </div>

            <button class="button" type="submit">Log In</button>
        </div>
    </form>
</body>
</html>
