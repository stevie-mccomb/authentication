<?php session_start();

    define('BASE_PATH', dirname(__DIR__));
    define('DATA_PATH', BASE_PATH . '/data');
    define('VIEW_PATH', BASE_PATH . '/views');

    global $users;
    $users = load();

    function redirect($path = '/')
    {
        header("Location: $path");
        exit;
    }

    function abort($code)
    {
        http_response_code($code);
        echo $code;
        exit;
    }

    function view($name)
    {
        $filename = VIEW_PATH . '/' . $name . '.php';

        if (file_exists($filename)) {
            require $filename;
        } else {
            abort(404);
        }
    }

    function user($email = false)
    {
        global $users;

        if (empty($email)) {
            $authenticatedID = (Int) ($_SESSION['user_id'] ?? 0);

            if (empty($authenticatedID)) return false;

            foreach ($users as $user) {
                if ($user->id === $authenticatedID) return $user;
            }
        } else {
            foreach ($users as $user) {
                if ($user->email === $email) return $user;
            }
        }

        return false;
    }

    function load()
    {
        $filename = DATA_PATH . '/users.json';

        if (!file_exists($filename)) file_put_contents($filename, json_encode([]));

        return json_decode(file_get_contents($filename));
    }

    function save($users)
    {
        file_put_contents(DATA_PATH . '/users.json', json_encode($users, JSON_PRETTY_PRINT));
    }

    function register()
    {
        //
    }

    function login()
    {
        //
    }

    function logout()
    {
        //
    }

    // Register a new user.
    if (!empty($_GET['action']) && $_GET['action'] === 'register') register();

    // Log out the current user.
    if (!empty($_GET['action']) && $_GET['action'] === 'logout') logout();

    // Attempt to log them in.
    if (!empty($_POST)) login();

    empty(user()) ? view('form') : view('secret');
